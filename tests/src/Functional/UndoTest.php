<?php

namespace Drupal\Tests\scanner\Functional;

/**
 * Tests the replacement functionality.
 *
 * @group scanner
 */
class UndoTest extends ScannerTestBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function setUp(): void {
    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Create a test content types.
    $this->createContentTypeNode('Title test', 'Body test', 'scanner_test_node_type', 'Scanner test node type');

    // Log in as an admin who can modify the module's settings.
    $user = $this->createUser(['administer scanner settings']);
    $this->drupalLogin($user);

    // Enable the content type.
    $this->drupalGet('admin/config/content/scanner');
    $session_assert = $this->assertSession();
    $session_assert->statusCodeEquals(200);
    $edit = [
      'enabled_content_types[node:scanner_test_node_type]' => 'node:scanner_test_node_type',
    ];
    $this->submitForm($edit, 'Save configuration');
    $edit = [
      'fields_of_selected_content_type[node:scanner_test_node_type:body]' => 'node:scanner_test_node_type:body',
    ];
    $this->submitForm($edit, 'Save configuration');
    $session_assert = $this->assertSession();
    $session_assert->statusCodeEquals(200);

    // Log in as a user that can use the replacement system.
    $user = $this->createUser(['administer nodes', 'perform search and replace']);
    $this->drupalLogin($user);
  }

  /**
   * Test coverage for the replacement system.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUndo() {
    $session_assert = $this->assertSession();
    $this->drupalGet('node/1');
    $session_assert->pageTextContains('Body test');

    // Load the main scanner form.
    $this->drupalGet('admin/content/scanner');
    $session_assert->statusCodeEquals(200);

    $this->submitForm(['search' => 'Body test', 'replace' => 'scanner'], 'Replace');
    $this->submitForm([], 'Confirm');
    $session_assert->pageTextContains('1 entities processed.');
    $this->drupalGet('node/1');
    $session_assert->pageTextNotContains('Body test');
    $session_assert->pageTextContains('scanner');

    $this->drupalGet('/admin/content/scanner/undo');
    $page = $this->getSession()->getPage();
    $page->clickLink('Undo');
    $this->submitForm([], 'Confirm');
    $session_assert->pageTextContains('Undone successful for ID: 1.');
    $this->drupalGet('node/1');
    $session_assert->pageTextContains('Body test');
  }

}
